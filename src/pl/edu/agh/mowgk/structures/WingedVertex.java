package pl.edu.agh.mowgk.structures;

/**
 * Created by gaba on 19.03.16.
 */
public class WingedVertex {

    private int id;

    private double x;
    private double y;
    private double z;
    private WingedEdge someEdge;

    public WingedVertex(int id, double x, double y, double z) {
        this.id = id;
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public int getId() {
        return id;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getZ() {
        return z;
    }


    public WingedEdge getSomeEdge() {
        return someEdge;
    }

    public void setSomeEdge(WingedEdge someEdge) {
        this.someEdge = someEdge;
    }

}
