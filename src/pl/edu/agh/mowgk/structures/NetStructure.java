package pl.edu.agh.mowgk.structures;

/**
 * Created by mapu on 19/03/16.
 */
public interface NetStructure {
    void setInitiatData(int vertexCount, int faceCount, int edgeCount);
    void setVertex(double x, double y, double z);
    boolean setFace(int vertexIndice1, int vertexIndice2, int vertexIndice3);
    void finalizeStructure();

//  1. dla każdego wierzchołka wyznaczanie otoczenia wierzchołków (pierwsza i druga warstwa sąsiednich wierzchołków),
//  2. dla każdego wierzchołka znalezienie elementów, do których należy,
//  3. dla każdego elementu wyznaczenie otoczenia elementów (pierwsza i druga warstwa sąsiednich elementów),
//  4. zamiana krawędzi dla wskazanej pary przyległych trójkątów wraz z odpowiednią zmianą struktury danych,
//  5. określenie, czy dana siatka posiada brzeg.

    void findSurroundingVertexesForAllVertexes();
    void findFacesForAllVertexes();
    void findSurroundingFacesForAllFaces();
    void changeEdgesForTriangles(int face1Number, int face2Number);
    void checkIfMeshHasMarrgin();


}
