package pl.edu.agh.mowgk.structures;

/**
 * Created by gaba on 19.03.16.
 */
public class WingedFace {
    private int id;
    private WingedEdge someEdge;

    public WingedFace(int id, WingedEdge someEdge) {
        this.id = id;
        this.someEdge = someEdge;
    }

    public WingedEdge getSomeEdge() {
        return someEdge;
    }

    public int getId() {
        return id;
    }

    public void setSomeEdge(WingedEdge someEdge) {
        this.someEdge = someEdge;
    }
}
