package pl.edu.agh.mowgk.structures;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by gaba on 19.03.16.
 */
public class WingedEdge {

    private int id;

    private WingedVertex v1;
    private WingedVertex v2;

    private WingedFace faceA;
    private WingedFace faceB;

    private WingedEdge edgePrevA;
    private WingedEdge edgeNextA;
    private WingedEdge edgePrevB;
    private WingedEdge edgeNextB;

    public WingedEdge(int id, WingedVertex vertex1, WingedVertex vertex2) {
        this.id = id;
        this.v1 = vertex1;
        this.v2 = vertex2;
    }

    public int getId() {
        return id;
    }

    public WingedVertex getV1() {
        return v1;
    }

    public WingedVertex getV2() {
        return v2;
    }

    public void setFace(WingedFace face) {
        if (this.faceA == null) {
            this.faceA = face;
        }else if (this.faceB == null){
            this.faceB = face;
        }else {
            //throw new Exception("third face?!?!");
            System.out.println("ERROR: third face?!?!?!");
        }
    }

    public void setFaceA(WingedFace face){
        this.faceA = face;
    }

    public void setFaceB(WingedFace face){
        this.faceB = face;
    }

    public WingedFace getFaceA() {
        return faceA;
    }

    public WingedFace getFaceB() {
        return faceB;
    }

    public WingedEdge getEdgePrevA() {
        return edgePrevA;
    }

    public WingedEdge getEdgeNextA() {
        return edgeNextA;
    }

    public WingedEdge getEdgePrevB() {
        return edgePrevB;
    }

    public WingedEdge getEdgeNextB() {
        return edgeNextB;
    }


    public List<WingedEdge> getNeighbours(){
        final LinkedList<WingedEdge> neighbours = new LinkedList<>();
        neighbours.add(edgePrevA);
        neighbours.add(edgeNextA);
        neighbours.add(edgePrevB);
        neighbours.add(edgeNextB);

        return neighbours;
    }


    public void setEdgePrevA(WingedEdge edgePrevA) {
        if (this.faceB == null) {
            this.edgePrevA = edgePrevA;
        } else {
            this.edgePrevB = edgePrevA;
        }
    }

    public void setEdgeNextA(WingedEdge edgeNextA) {
        if (this.faceB == null) {
            this.edgeNextA = edgeNextA;
        } else {
            this.edgeNextB = edgeNextA;
        }
    }

    public void setEdgePrevA(WingedEdge edgePrevA, boolean absoluteA) {
        if ( absoluteA ){
            this.edgePrevA= edgePrevA;
        } else setEdgePrevA(edgePrevA);
    }

    public void setEdgeNextA(WingedEdge edgeNextA, boolean absoluteA) {
        if ( absoluteA ){
            this.edgeNextA = edgeNextA;
        } else setEdgeNextA(edgeNextA);
    }

    public void setEdgePrevB(WingedEdge edgePrevB) {
        this.edgePrevB = edgePrevB;
    }

    public void setEdgeNextB(WingedEdge edgeNextB) {
        this.edgeNextB = edgeNextB;
    }

    public boolean compare(WingedEdge edge) {

        boolean firstPair = this.v1 == edge.getV1() && this.v2 == edge.getV2();
        boolean secondPair = this.v1 == edge.getV2() && this.v2 == edge.getV1();
        return firstPair || secondPair;

    }

    public void setV1(WingedVertex v1) {
        this.v1 = v1;
    }

    public void setV2(WingedVertex v2) {
        this.v2 = v2;
    }
}
