package pl.edu.agh.mowgk.structures;

import java.util.*;

/**
 * Created by gaba on 18.03.16.
 */
public class WingedEdgesDS implements NetStructure{

    private Map<Integer, WingedEdge> edges;
    private Map<Integer, WingedVertex> vertices;
    private Map<Integer, WingedFace> faces;


    public WingedEdgesDS(){
        this.edges = new HashMap<>();
        this.vertices = new HashMap<>();
        this.faces = new HashMap<>();
    }

    // build structure region
    @Override
    public void setInitiatData(int vertexCount, int faceCount, int edgeCount) {

    }

    @Override
    public void setVertex(double x, double y, double z) {
        //NOTE: I guess, there should be some checking if vertex do not exists already
        final int id = vertices.size();
        vertices.put(id, new WingedVertex(id, x, y, z));

    }

    @Override
    public boolean setFace(int vertexIndice1, int vertexIndice2, int vertexIndice3) {
        final WingedVertex wingedV1 = vertices.get(vertexIndice1);
        final WingedVertex wingedV2 = vertices.get(vertexIndice2);
        final WingedVertex wingedV3 = vertices.get(vertexIndice3);

        if (wingedV1 == null) return false;
        if (wingedV2 == null) return false;
        if (wingedV3 == null) return false;

        final WingedEdge edge1 = addEdge(wingedV1, wingedV2);
        final WingedEdge edge2 = addEdge(wingedV2, wingedV3);
        final WingedEdge edge3 = addEdge(wingedV3, wingedV1);

        final int faceID = faces.size();
        final WingedFace face = new WingedFace(faceID, edge1);
        faces.put(faceID, face);

        //if one face is already set, the next face will be faceB...
        edge1.setFace(face);
        edge2.setFace(face);
        edge3.setFace(face);

        //..and the same with Edge{Prev,Next}
        edge1.setEdgePrevA(edge2);
        edge1.setEdgeNextA(edge3);
        edge2.setEdgePrevA(edge3);
        edge2.setEdgeNextA(edge1);
        edge3.setEdgePrevA(edge1);
        edge3.setEdgeNextA(edge2);

        return true;
    }

    @Override
    public void finalizeStructure(){

//        System.out.println("finalize");

        for(Map.Entry<Integer, WingedEdge> entry : edges.entrySet()) {
            WingedEdge edge = entry.getValue();
            if (edge.getFaceB() == null){

                final WingedEdge neighbour = findNeighbour(edge, edge.getV1());

//                System.out.println("neighbour one:" + neighbour);
                edge.setEdgePrevB(neighbour);

                final WingedEdge neighbour2 = findNeighbour(edge, edge.getV2());
//                System.out.println("neighbour two:" + neighbour2);
                edge.setEdgeNextB(neighbour2);

            }

        }
    }

    private WingedEdge findNeighbour(WingedEdge edge, WingedVertex vertex) {
        WingedFace searchingFace = edge.getFaceA();

        do {
            WingedEdge possibleNeighbour = null;

            for (Map.Entry<Integer, WingedEdge> entry : edges.entrySet()) {
                WingedEdge nextEdge = entry.getValue();
                if (nextEdge == edge) continue;
                if (nextEdge.getFaceA() == searchingFace || nextEdge.getFaceB() == searchingFace) {
                    if (nextEdge.getV1() == vertex || nextEdge.getV2() == vertex) {
                        possibleNeighbour = nextEdge;
                        break;
                    }
                }
            }

            if (possibleNeighbour == null) {
                System.out.println("Something went wrong in finalize");
                return null;
            }

            searchingFace = (possibleNeighbour.getFaceB() == searchingFace) ?
                    possibleNeighbour.getFaceA() : possibleNeighbour.getFaceB();

            if (searchingFace == null) {
                return possibleNeighbour;
            }

            edge = possibleNeighbour;

        // maybe put here something to prevent infinite loop
        // for bad structure
        } while(true);

    }


    private WingedEdge addEdge(WingedVertex vertex1, WingedVertex vertex2){

        final int edgeID = edges.size();
        WingedEdge edge = new WingedEdge(edgeID, vertex1, vertex2);

        boolean edgeExist = false;
        for(Map.Entry<Integer, WingedEdge> entry : edges.entrySet()) {
            WingedEdge value = entry.getValue();
            if (value.compare(edge)){
                edge = value;
                edgeExist = true;
                break;
            }
        }

        if (edge.getV1().getSomeEdge() == null) {
            edge.getV1().setSomeEdge(edge);
        }
        if (edge.getV2().getSomeEdge() == null) {
            edge.getV2().setSomeEdge(edge);
        }

        if(!edgeExist){
            edges.put(edgeID, edge);
        }

        return edge;

    }
    // end build structure region


    // structure operations region

    /**
     * dla każdego wierzchołka wyznaczanie otoczenia wierzchołków
     * (pierwsza i druga warstwa sąsiednich wierzchołków)
     */
    @Override
    public void findSurroundingVertexesForAllVertexes() {

        Map<WingedVertex, Set<WingedVertex>> verticesAroundVertices = new HashMap<>();

        for (Map.Entry<Integer, WingedVertex> vertexEntry : vertices.entrySet()) {

            final WingedVertex vertex = vertexEntry.getValue();
            Set<WingedVertex> verticesAroundVertex = new HashSet<>();
            verticesAroundVertices.put(vertex, verticesAroundVertex);

            findSurroundingVertices(vertex, verticesAroundVertex, 1);
            verticesAroundVertex.remove(vertex);
        }


        //print out result
//        for (Map.Entry<WingedVertex, Set<WingedVertex>> verticesAroundVertexEntry
//                : verticesAroundVertices.entrySet()) {
//
//            final Set<WingedVertex> verticesAroundVertex = verticesAroundVertexEntry.getValue();
//
//            System.out.println("Wierzchołki otaczające wierzchołek "
//                    + verticesAroundVertexEntry.getKey().getId());
//            for (WingedVertex vertexVertex : verticesAroundVertex) {
//                System.out.println("\t" + vertexVertex.getId());
//            }
//        }

    }

    private void findSurroundingVertices(WingedVertex vertex, Set<WingedVertex> verticesAroundVertex,
                                         int recursionCount){
        for (Map.Entry<Integer, WingedEdge> edgeEntry : edges.entrySet()) {

            final WingedEdge edge = edgeEntry.getValue();

            final WingedVertex edgeV1 = edge.getV1();
            final WingedVertex edgeV2 = edge.getV2();

            if(edgeV1 == vertex){
                verticesAroundVertex.add(edgeV2);
                if (recursionCount > 0) {
                    findSurroundingVertices(edgeV2, verticesAroundVertex, recursionCount - 1);
                }
            }
            if(edgeV2 == vertex){
                verticesAroundVertex.add(edgeV1);
                if (recursionCount > 0) {
                    findSurroundingVertices(edgeV1, verticesAroundVertex, recursionCount - 1);
                }
            }

        }

    }

    /**
     * dla kazdego wierzcholka znalezienie elementow, do ktorych nalezy,
     */
    @Override
    public void findFacesForAllVertexes() {

        Map<WingedVertex, Set<WingedFace>> verticesFaces = new HashMap<>();

        for (Map.Entry<Integer, WingedVertex> vertexEntry : vertices.entrySet()) {

            final WingedVertex vertex = vertexEntry.getValue();

            Set<WingedFace> vertexFacesSet = new HashSet<>();
            verticesFaces.put(vertex, vertexFacesSet);

            for (Map.Entry<Integer, WingedEdge> edgeEntry : edges.entrySet()) {
                final WingedEdge edge = edgeEntry.getValue();

                if (edge.getV1() == vertex || edge.getV2() == vertex) {
                    WingedFace faceToAdd = edge.getFaceA();
                    if (faceToAdd != null) vertexFacesSet.add(faceToAdd);
                    faceToAdd = edge.getFaceB();
                    if (faceToAdd != null) vertexFacesSet.add(faceToAdd);

                }

            }
        }

        //print out result
//        for (Map.Entry<WingedVertex, Set<WingedFace>> vertexFacesEntry : verticesFaces.entrySet()) {
//            final Set<WingedFace> vertexFaces = vertexFacesEntry.getValue();
//            System.out.println("Elementy, do ktorych nalezy wierzcholek " + vertexFacesEntry.getKey().getId());
//            for (WingedFace vertexFace : vertexFaces) {
//                System.out.println("\t" + vertexFace.getId());
//            }
//        }
    }

    /**
     * dla kazdego elementu wyznaczenie otoczenia elementow
     * (pierwsza i druga warstwa sasiednich elementow),
     * przyleganie scianami
     */
    @Override
    public void findSurroundingFacesForAllFaces() {

        Map<WingedFace, Set<WingedFace>> facesFaces = new HashMap<>();

        for (Map.Entry<Integer, WingedFace> faceEntry : faces.entrySet()) {
            final Integer faceID = faceEntry.getKey();
            final WingedFace face = faceEntry.getValue();

            Set<WingedFace> faceFaces = new HashSet<>();
            facesFaces.put(face, faceFaces);

            findSurroundingFacesForFace(face, faceFaces, 1);
            faceFaces.remove(face);
        }


        //print out result
//        for (Map.Entry<WingedFace, Set<WingedFace>> faceFacesEntry : facesFaces.entrySet()) {
//            final Set<WingedFace> faceFaces = faceFacesEntry.getValue();
//            System.out.println("Elementy otaczające element " + faceFacesEntry.getKey().getId());
//            for (WingedFace face : faceFaces) {
//                System.out.println("\t" + face.getId());
//            }
//        }
    }

    public void findSurroundingFacesForFace(WingedFace face, Set<WingedFace> faceFaces, int recursionCount){

        for (Map.Entry<Integer, WingedEdge> edgeEntry : edges.entrySet()) {

            final WingedEdge edge = edgeEntry.getValue();

            final WingedFace edgeF1 = edge.getFaceA();
            final WingedFace edgeF2 = edge.getFaceB();

            if(edgeF1 != null && edgeF1 == face){
                if(edgeF2 != null) {
                    faceFaces.add(edgeF2);
                    if(recursionCount > 0){
                        findSurroundingFacesForFace(edgeF2, faceFaces, recursionCount - 1);
                    }
                }
            }
            if(edgeF2 != null && edgeF2 == face){
                if(edgeF1 != null) {
                    faceFaces.add(edgeF1);
                    if(recursionCount > 0){
                        findSurroundingFacesForFace(edgeF1, faceFaces, recursionCount - 1);
                    }
                }
            }

        }
    }

    /**
     * zamiana krawędzi dla wskazanej pary przyległych trójkątów
     * wraz z odpowiednią zmianą struktury danych
     */
    @Override
    public void changeEdgesForTriangles(int face1Number, int face2Number) {
        //TODO: add check if change can be done

        final WingedFace face1 = faces.get(face1Number);
        final WingedFace face2 = faces.get(face2Number);

        Set<WingedVertex> facesVertexes = new HashSet<>();
        WingedEdge facesEdge = null;

        //find edge between face1 and face2
        for (Map.Entry<Integer, WingedEdge> edgeEntry : edges.entrySet()) {
            final WingedEdge edge = edgeEntry.getValue();

            final WingedFace edgeF1 = edge.getFaceA();
            final WingedFace edgeF2 = edge.getFaceB();

            if ((edgeF1 == face1 || edgeF1 == face2) ||
                    (edgeF2 == face1 || edgeF2 == face2)) {
                facesVertexes.add(edge.getV1());
                facesVertexes.add(edge.getV2());
            }

            // skip outher edges
            if (edgeF2 == null) continue;

            if ((edgeF1 == face1 && edgeF2 == face2) ||
                    (edgeF2 == face1 && edgeF1 == face2)) {
                facesEdge = edge;
                break;
            }
        }

        if (facesEdge == null) {
            System.out.println("ERROR: in changeEdgesForTriangles");
            return;
        }

        //prepare some helpful variables
        final WingedVertex facesEdgeV1 = facesEdge.getV1();
        final WingedVertex facesEdgeV2 = facesEdge.getV2();


        //reset faces pointer to edge
        face1.setSomeEdge(facesEdge);
        face2.setSomeEdge(facesEdge);


        //compare directions in faces
        final WingedEdge edgePrevA = facesEdge.getEdgePrevA();
        final WingedEdge edgeNextA = facesEdge.getEdgeNextA();
        final WingedEdge edgePrevB = facesEdge.getEdgePrevB();
        final WingedEdge edgeNextB = facesEdge.getEdgeNextB();

        boolean sameDirections;
        if (edgeNextB.getV1() == edgeNextA.getV1() ||
                edgeNextB.getV2() == edgeNextA.getV2() ||
                edgeNextB.getV1() == edgeNextA.getV2() ||
                edgeNextB.getV2() == edgeNextA.getV1() ){
            sameDirections = true;
        } else{
            sameDirections = false;
        }

        //reset neighbours
        if (sameDirections){
            facesEdge.setEdgeNextA(edgePrevA, true);
            facesEdge.setEdgePrevB(edgeNextA);

            if (edgePrevA.getFaceA() == facesEdge.getFaceA()){
                edgePrevA.setEdgePrevA(facesEdge, true);
                edgePrevA.setEdgeNextA(edgePrevB, true);
            } else {
                edgePrevA.setEdgePrevB(facesEdge);
                edgePrevA.setEdgeNextB(edgePrevB);
            }

            if (edgeNextA.getFaceA() == facesEdge.getFaceA()){
                edgeNextA.setEdgePrevA(edgeNextB, true);
                edgeNextA.setEdgeNextA(facesEdge, true);
            } else {
                edgeNextA.setEdgePrevB(edgeNextB);
                edgeNextA.setEdgeNextB(facesEdge);
            }

            if (edgePrevB.getFaceA() == facesEdge.getFaceB()){
                edgePrevB.setEdgePrevA(edgePrevA, true);
            } else {
                edgePrevB.setEdgePrevB(edgePrevA);
            }

            if (edgeNextB.getFaceA() == facesEdge.getFaceB()){
                edgeNextB.setEdgeNextA(edgeNextA, true);
            } else {
                edgeNextB.setEdgeNextB(edgeNextA);
            }
        } else {
            facesEdge.setEdgePrevA(edgePrevB, true);
            facesEdge.setEdgeNextA(edgeNextA, true);
            facesEdge.setEdgePrevB(edgeNextB);
            facesEdge.setEdgeNextB(edgePrevA);

            if (edgePrevA.getFaceA() == facesEdge.getFaceA()){
                edgePrevA.setEdgePrevA(edgeNextB, true);
            } else {
                edgePrevA.setEdgePrevB(edgeNextB);
            }

            if (edgeNextA.getFaceA() == facesEdge.getFaceA()){
                edgeNextA.setEdgeNextA(edgePrevB, true);
            } else {
                edgeNextA.setEdgeNextB(edgePrevB);
            }

            if (edgePrevB.getFaceA() == facesEdge.getFaceB()){
                edgePrevB.setEdgePrevA(edgeNextA, true);
            } else {
                edgePrevB.setEdgePrevB(edgeNextA);
            }

            if (edgeNextB.getFaceA() == facesEdge.getFaceB()){
                edgeNextB.setEdgeNextA(edgePrevA, true);
            } else {
                edgeNextB.setEdgeNextB(edgePrevA);
            }
        }


        //reset neighbours' faces
        if (edgeNextA.getFaceA() == facesEdge.getFaceA()){
            edgeNextA.setFaceA(facesEdge.getFaceB());
        } else { //faceB of edgeNextA is == of faceA in our middle edge
            edgeNextA.setFaceB(facesEdge.getFaceB());
        }

        if (sameDirections){
            if (edgePrevB.getFaceA() == facesEdge.getFaceB()){
                edgePrevB.setFaceA(facesEdge.getFaceA());
            } else { //faceB of edgeNextA is == of faceA in our middle edge
                edgePrevB.setFaceB(facesEdge.getFaceA());
            }
        } else {
            if (edgeNextB.getFaceA() == facesEdge.getFaceB()){
                edgeNextB.setFaceA(facesEdge.getFaceA());
            } else { //faceB of edgeNextA is == of faceA in our middle edge
                edgeNextB.setFaceB(facesEdge.getFaceA());
            }
        }


//
//        System.out.println("After changing " + face1Number + " and " + face2Number + " faces!!!");
//        System.out.println("Original edge ID: " + facesEdge.getId());
//        System.out.println(this);
    }

    /**
     * określenie, czy dana siatka posiada brzeg.
     */
    @Override
    public void checkIfMeshHasMarrgin() {

        for(Map.Entry<Integer, WingedEdge> entry : edges.entrySet()) {
            WingedEdge edge = entry.getValue();

            if(edge.getFaceB() == null){
//                System.out.println("Istnieje brzeg");
                return;
            }

        }

//        System.out.println("Nie istnieje brzeg");

    }
    // end structure operations region


    @Override
    public String toString(){

        StringBuilder result = new StringBuilder();
        String NEW_LINE = System.getProperty("line.separator");

        result.append("Edge table:" + NEW_LINE);
        result.append("ID:\tV:\t\tF:\t\tneighbours" + NEW_LINE);

        for (Map.Entry<Integer, WingedEdge> entry : edges.entrySet()) {
            int edgeID = entry.getKey();
            WingedEdge edge = entry.getValue();

            final WingedFace f2 = edge.getFaceB();
            String f2Id;
            if (f2 != null){
                f2Id = "" + f2.getId();
            } else {
                f2Id = "-";
            }
            final WingedEdge e3 = edge.getEdgePrevB();
            String e3Id;
            if (e3 != null){
                e3Id = "" + e3.getId();
            } else {
                e3Id = "-";
            }
            final WingedEdge e4 = edge.getEdgeNextB();
            String e4Id;
            if (e4 != null){
                e4Id = "" + e4.getId();
            } else {
                e4Id = "-";
            }



            result.append(edgeID + "\t")
                    .append(edge.getV1().getId() + "\t")
                    .append(edge.getV2().getId() + "\t")
                    .append(edge.getFaceA().getId() + "\t")
                    .append(f2Id + "\t")
                    .append(edge.getEdgePrevA().getId() + "\t")
                    .append(edge.getEdgeNextA().getId() + "\t")
                    .append(e3Id + "\t")
                    .append(e4Id + "\t");

            result.append(NEW_LINE);
        }


        result.append("Vertex table:" + NEW_LINE);
        result.append("ID:\tx:\ty:\tz:\tsome edge:" + NEW_LINE);

        for (Map.Entry<Integer, WingedVertex> entry : vertices.entrySet()) {
            int vertexID = entry.getKey();
            WingedVertex vertex = entry.getValue();

            final WingedEdge someEdge = vertex.getSomeEdge();
            String someEdgeId;
            if (someEdge != null){
                someEdgeId = "" + someEdge.getId();
            } else {
                someEdgeId = "-";
            }

            result.append(vertexID + "\t")
                    .append(vertex.getX() + "\t")
                    .append(vertex.getY() + "\t")
                    .append(vertex.getZ() + "\t")
                    .append(someEdgeId);

            result.append(NEW_LINE);
        }


        result.append("Face table:" + NEW_LINE);
        result.append("ID:\tsome edge" + NEW_LINE);

        for (Map.Entry<Integer, WingedFace> entry : faces.entrySet()) {
            int faceID = entry.getKey();
            WingedFace face = entry.getValue();
            result.append(faceID + "\t")
                    .append(face.getSomeEdge().getId() + "\t");

            result.append(NEW_LINE);
        }


        return result.toString();
    }

}
