package pl.edu.agh.mowgk.structures;

/**
 * Created by mapu on 19/03/16.
 */
public class SimpleFace {
    private int vertex1;
    private int vertex2;
    private int vertex3;
    private int faceNumber;

    public SimpleFace(int vertex1, int vertex2, int vertex3, int faceNumber) {
        this.vertex1 = vertex1;
        this.vertex2 = vertex2;
        this.vertex3 = vertex3;
        this.faceNumber = faceNumber;
    }


    public int getVertex1() {
        return vertex1;
    }

    public int getVertex2() {
        return vertex2;
    }

    public int getVertex3() {
        return vertex3;
    }

    public int getFaceNumber() {
        return faceNumber;
    }

    public boolean containsVertex(int currentNumber) {
        if(vertex1 == currentNumber){
            return true;
        } else if(vertex2 == currentNumber){
            return true;
        } else if(vertex3 == currentNumber){
            return true;
        }
        return false;
    }

    public boolean isEqual(SimpleFace face){
        if(face.getFaceNumber() == faceNumber){
            return true;
        } else {
            return false;
        }
    }

    public int[] getOtherVertexes(int vertex){
        int[] vertexes = new int[2];
        int currentVertex = 0;
        if(vertex1 != vertex){
            vertexes[currentVertex] = vertex1;
            currentVertex++;
        }
        if(vertex2 != vertex){
            vertexes[currentVertex] = vertex2;
            currentVertex++;
        }
        if(vertex3 != vertex){
            vertexes[currentVertex] = vertex3;
            currentVertex++;
        }

        return vertexes;
    }

    public boolean hasTheSameEdge(SimpleFace currentFace) {
//        System.out.println("v: " + vertex1 + ", " + vertex2 + ", " + vertex3 + " | " + currentFace.getVertex1() + ", " + currentFace.getVertex2() +  ", " + currentFace.getVertex3());

        int numberOfSharedVertexes = 0;

        if(currentFace.getVertex1() == vertex1 || currentFace.getVertex1() == vertex2 || currentFace.getVertex1() == vertex3){
            numberOfSharedVertexes++;
        }

        if(currentFace.getVertex2() == vertex1 || currentFace.getVertex2() == vertex2 || currentFace.getVertex2() == vertex3){
            numberOfSharedVertexes++;
        }

        if(currentFace.getVertex3() == vertex1 || currentFace.getVertex3() == vertex2 || currentFace.getVertex3() == vertex3){
            numberOfSharedVertexes++;
        }

        if(numberOfSharedVertexes == 2){
            return true;
        }

        return false;
    }

    public int addVertexSaveOther(int vertexToAdd, int vertexToSave) {
        int replacedVertex = -1;
        if(vertex1 != vertexToSave && vertex1 != vertexToAdd){
            replacedVertex = vertex1;
            vertex1 = vertexToAdd;
            return replacedVertex;
        }

        if(vertex2 != vertexToSave && vertex2 != vertexToAdd){
            replacedVertex = vertex2;
            vertex2 = vertexToAdd;
            return replacedVertex;
        }

        if(vertex3 != vertexToSave && vertex3 != vertexToAdd){
            replacedVertex = vertex3;
            vertex3 = vertexToAdd;
            return replacedVertex;
        }

        return replacedVertex;
    }

    public void addVertexSaveTwo(int vertexToAdd, int vertex1ToSave, int vertex2ToSave) {

        if(vertex1 != vertex1ToSave && vertex1 != vertex2ToSave){
            vertex1 = vertexToAdd;
        }

        if(vertex2 != vertex1ToSave && vertex2 != vertex2ToSave){
            vertex2 = vertexToAdd;
        }

        if(vertex3 != vertex1ToSave && vertex3 != vertex2ToSave){
            vertex3 = vertexToAdd;
        }

    }

    public boolean hasEdge(int vertex1, int vertex2) {
        int hasEdge = 0;
        if(this.vertex1 == vertex1 || this.vertex2 == vertex1 || vertex3 == vertex1){
            hasEdge++;
        }

        if(this.vertex1 == vertex2 || this.vertex2 == vertex2 || vertex3 == vertex2){
            hasEdge++;
        }

        if(hasEdge == 2){
            return true;
        }

        return false;
    }
}
