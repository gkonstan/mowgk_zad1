package pl.edu.agh.mowgk.structures;

/**
 * Created by mapu on 19/03/16.
 */
public class SimpleVertex {
    private double x;
    private double y;
    private double z;

    public SimpleVertex(double x, double y, double z){
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getZ() {
        return z;
    }


}
