package pl.edu.agh.mowgk.structures;

import java.util.*;

/**
 * Created by mapu on 19/03/16.
 */
public class SimpleStructure implements NetStructure {

    //max size: 2 147 483 643
    private SimpleVertex[] vertexesTable;
    private SimpleFace[] facesTable;

    private int nuberOfVertexes;
    private int nuberOfFaces;

    public SimpleStructure(){

    }

    @Override
    public void setInitiatData(int vertexCount, int faceCount, int edgeCount) {
        vertexesTable = new SimpleVertex[vertexCount];
        facesTable = new SimpleFace[faceCount];

    }

    @Override
    public void setVertex(double x, double y, double z) {
        vertexesTable[nuberOfVertexes] = new SimpleVertex(x, y, z);
        nuberOfVertexes++;
    }

    @Override
    public boolean setFace(int vertexIndice1, int vertexIndice2, int vertexIndice3) {
        facesTable[nuberOfFaces] = new SimpleFace(vertexIndice1, vertexIndice2, vertexIndice3, nuberOfFaces);
//        System.out.println("-> " + facesTable[nuberOfFaces].getFaceNumber() + " | " + facesTable[nuberOfFaces].getVertex1() + ", " + facesTable[nuberOfFaces].getVertex2() + ", " + facesTable[nuberOfFaces].getVertex3());
        nuberOfFaces++;
        return true;
    }

    @Override
    public void finalizeStructure() {

    }

    /**
     * dla kazdego wierzcholka wyznaczanie otoczenia wierzcholkow (pierwsza i druga warstwa sasiednich wierzcholkow).
     */
    @Override
    public void findSurroundingVertexesForAllVertexes(){
//        Map<SimpleVertex, Set<SimpleVertex>> surroundingVertexes = new Hashtable<SimpleVertex, Set<SimpleVertex>>();

        for(int currentVertexNumber = 0; currentVertexNumber < nuberOfVertexes - 1; currentVertexNumber++){
            List<SimpleFace> closestFaces = new LinkedList<SimpleFace>();
            for(SimpleFace currentFace : facesTable){
                if(currentFace.containsVertex(currentVertexNumber)){
                    closestFaces.add(currentFace);
                }
            }

            Set<Integer> closestVertexes = new HashSet<Integer>();
            for(SimpleFace currentFace : closestFaces){
                if(!currentFace.containsVertex(currentVertexNumber)){
                    System.out.println("Error in algorithm.");
                    return;
                }

                int [] otherVertexes = currentFace.getOtherVertexes(currentVertexNumber);
                closestVertexes.add(otherVertexes[0]);
                closestVertexes.add(otherVertexes[1]);
            }

            for(Integer vertex : closestVertexes){
                for(SimpleFace currentFace : facesTable){
                    if(currentFace.containsVertex(vertex.intValue())){
                        closestFaces.add(currentFace);
                    }
                }
            }

            for(SimpleFace currentFace : closestFaces){
                closestVertexes.add(currentFace.getVertex1());
                closestVertexes.add(currentFace.getVertex2());
                closestVertexes.add(currentFace.getVertex3());
            }

            // usuwam element dla ktorego sprwadzamy - dodany przez poprzednia akcje.
            // zamiast tego mozna dac 3 ify w poprzedniej petli - if(vertex1 != currentVertexNumber) -> add to set
            closestVertexes.remove(currentVertexNumber);

//            System.out.print("Vertexes:");
//            for(Integer vertex : closestVertexes){
//                System.out.print(vertex.intValue() + " ");
//            }
//            System.out.println();
        }
    }

    /**
     * dla kazdego wierzcholka znalezienie elementow, do ktorych nalezy,
     */
    @Override
    public void findFacesForAllVertexes(){
        for(int currentVertexNumber = 0; currentVertexNumber < nuberOfVertexes - 1; currentVertexNumber++){
            List<SimpleFace> containingFaces = new LinkedList<SimpleFace>();

            for(SimpleFace currentFace : facesTable){
                if(currentFace.containsVertex(currentVertexNumber)){
                    containingFaces.add(currentFace);
                }
            }

//            System.out.print("Faces for " + currentVertexNumber + ": ");
//            for(SimpleFace currentFace : containingFaces){
//                System.out.print(currentFace.getFaceNumber() + " ");
//            }
//            System.out.println();
        }
    }

    /**
     * dla kazdego elementu wyznaczenie otoczenia elementow (pierwsza i druga warstwa sasiednich elementow),
     * przyleganie scianami
     */
    @Override
    public void findSurroundingFacesForAllFaces(){
        for(SimpleFace currentFace: facesTable){
            Set<SimpleFace> surroundingFaces = new HashSet<SimpleFace>();

            List<SimpleFace> closestFaces = new LinkedList<SimpleFace>();
            for(SimpleFace face : facesTable){
                if(currentFace.isEqual(face)){
                    continue;
                }

                if(face.hasTheSameEdge(currentFace)){
                    closestFaces.add(face);
                    surroundingFaces.add(face);
                }
            }

            for(SimpleFace closesFace : closestFaces){
                for(SimpleFace face : facesTable){
                    if(face.isEqual(currentFace)){
                        continue;
                    }

                    if(closesFace.hasTheSameEdge(face)){
                        surroundingFaces.add(face);
                    }
                }
            }

//            System.out.print("Surrounding for " + currentFace.getFaceNumber() + ": ");
//            for(SimpleFace face : surroundingFaces){
//                System.out.print(face.getFaceNumber() + " ");
//            }
//            System.out.println();
        }
    }

    @Override
    public void changeEdgesForTriangles(int face1Number, int face2Number) {
        SimpleFace face1 = facesTable[face1Number];
        SimpleFace face2 = facesTable[face2Number];

        if(!face1.hasTheSameEdge(face2)){
            System.out.println("Error, no common edge.");
            return;
        }

        // Znalezc ten ktory sie nie zawiera
        int vertexInFace1Only = -1;
        if(!face2.containsVertex(face1.getVertex1())){
            vertexInFace1Only = face1.getVertex1();
        }else if(!face2.containsVertex(face1.getVertex2())){
            vertexInFace1Only = face1.getVertex2();
        }else if(!face2.containsVertex(face1.getVertex3())){
            vertexInFace1Only = face1.getVertex3();
        }

        int vertexInFace2Only = -1;
        if(!face1.containsVertex(face2.getVertex1())){
            vertexInFace2Only = face2.getVertex1();
        }else if(!face1.containsVertex(face2.getVertex2())){
            vertexInFace2Only = face2.getVertex2();
        }else if(!face1.containsVertex(face2.getVertex3())){
            vertexInFace2Only = face2.getVertex3();
        }

        int [] theSameVertexes = face1.getOtherVertexes(vertexInFace1Only);

//        System.out.println("Before");
//        System.out.println("Face: " + face1.getFaceNumber() + " | " + face1.getVertex1() + ", " + face1.getVertex2() + ", " + face1.getVertex3());
//        System.out.println("Face: " + face2.getFaceNumber()  + " | " + face2.getVertex1() + ", " + face2.getVertex2() + ", " + face2.getVertex3());

        int removedVertex = face1.addVertexSaveOther(vertexInFace2Only, vertexInFace1Only);
        face2.addVertexSaveTwo(vertexInFace1Only, vertexInFace2Only, removedVertex);


//        System.out.println("After");
//        System.out.println("Face: " + face1.getFaceNumber() + " | " + face1.getVertex1() + ", " + face1.getVertex2() + ", " + face1.getVertex3());
//        System.out.println("Face: " + face2.getFaceNumber()  + " | " + face2.getVertex1() + ", " + face2.getVertex2() + ", " + face2.getVertex3());

    }


    /**
     * określenie, czy dana siatka posiada brzeg.
     */
    @Override
    public void checkIfMeshHasMarrgin(){
        List<String> borderEdges = new LinkedList<String>();
        for(SimpleFace currentFace : facesTable){

            int vertex1 = currentFace.getVertex1();
            int vertex2 = currentFace.getVertex2();
            int vertex3 = currentFace.getVertex3();

            boolean edge1Found = false;
            boolean edge2Found = false;
            boolean edge3Found = false;

            for(SimpleFace face : facesTable){
                if(currentFace.isEqual(face)){
                    continue;
                }

                if(face.hasEdge(vertex1, vertex2)){
                    edge1Found = true;
                }
                if(face.hasEdge(vertex2, vertex3)){
                    edge2Found = true;
                }
                if(face.hasEdge(vertex3, vertex1)){
                    edge3Found = true;
                }
            }

            if(!edge1Found){
                borderEdges.add("" + vertex1 +"-" + vertex2);
            }

            if(!edge2Found){
                borderEdges.add("" + vertex2 +"-" + vertex3);
            }

            if(!edge3Found){
                borderEdges.add("" + vertex3 +"-" + vertex1);
            }
        }

//        for(String border : borderEdges){
//            System.out.println(border);
//        }
    }



    public int getNuberOfFaces() {
        return nuberOfFaces;
    }

    public int getNuberOfVertexes() {
        return nuberOfVertexes;
    }



}
