package pl.edu.agh.mowgk.parser;

import pl.edu.agh.mowgk.structures.NetStructure;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

/**
 * Created by gaba on 19.03.16.
 *
 * It's very simplified parser - the header in .ply file almost doesn't matter
 */
public class PlyFileParser implements FileParser {


    private String filePath;
    private NetStructure structure;

    private int vertexCount;
    private int faceCount;

    private String formatError = "PLY file format error.";

    @Override
    public void loadFileIntoStructure(String filePath, NetStructure structure) throws IOException {

        this.filePath = filePath;
        this.structure = structure;

        File plyFile = new File(filePath);
        if(!plyFile.exists() || plyFile.isDirectory()) {
            System.out.println("There is no such file.");
            return;
        }

        FileInputStream inputStream = null;
        Scanner scanner = null;
        try {
            inputStream = new FileInputStream(filePath);
            scanner = new Scanner(inputStream, "UTF-8");

            String line = null;
            if (scanner.hasNextLine()){
                line = scanner.nextLine();
            }

            if(line == null || line.compareTo("ply") != 0) {
                System.out.println(formatError);
                return;
            }

            if(!readHeader(scanner)){
                System.out.println(formatError);
                return;
            }

            if(!readBody(scanner)){
                System.out.println(formatError);
                return;
            }

            structure.finalizeStructure();

//            System.out.println(structure);

            // note that Scanner suppresses exceptions
            if (scanner.ioException() != null) {
                throw scanner.ioException();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (inputStream != null) {
                inputStream.close();
            }
            if (scanner != null) {
                scanner.close();
            }
        }

    }

    private boolean readHeader(Scanner scanner) {
        String line = "";

        //search for vertex info
        while (scanner.hasNextLine()) {
            line = scanner.nextLine();
            if(line.startsWith("element vertex")){
                break;
            }
        }

        if(line.startsWith("element vertex")){
            String[] vertexInfo = line.split(" ");
            if(vertexInfo.length != 3){
                return false;
            }

            vertexCount = Integer.parseInt(vertexInfo[2]);
        }


        //search for face info
        while (scanner.hasNextLine()) {
            line = scanner.nextLine();
            if(line.startsWith("element face")){
                break;
            }
        }

        if(line.startsWith("element face")){
            String[] faceInfo = line.split(" ");
            if(faceInfo.length != 3){
                return false;
            }

            faceCount = Integer.parseInt(faceInfo[2]);
        }

        //search for end of header
        while (scanner.hasNextLine()) {
            line = scanner.nextLine();
            if(line.startsWith("end_header")){
                break;
            }
        }

        structure.setInitiatData(vertexCount, faceCount, 0);

        return true;
    }


    private boolean readBody(Scanner scanner) {

        String line;

        //vertices
        while (scanner.hasNextLine() && vertexCount > 0) {
            line = scanner.nextLine();
            String[] vertexInfo = line.split(" ");
            if(vertexInfo.length != 3){
                return false;
            }

            double x = Double.parseDouble(vertexInfo[0]);
            double y = Double.parseDouble(vertexInfo[1]);
            double z = Double.parseDouble(vertexInfo[2]);

            structure.setVertex(x, y, z);

            vertexCount--;
        }

        //faces
        while (scanner.hasNextLine() && faceCount > 0) {
            line = scanner.nextLine();
            String[] faceInfo = line.split(" ");
            if(faceInfo.length != 4){
                return false;
            }

            int v1 = Integer.parseInt(faceInfo[1]);
            int v2 = Integer.parseInt(faceInfo[2]);
            int v3 = Integer.parseInt(faceInfo[3]);

            structure.setFace(v1, v2, v3);

            faceCount--;
        }

        return true;
    }

}
