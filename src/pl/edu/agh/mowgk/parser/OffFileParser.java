package pl.edu.agh.mowgk.parser;

import pl.edu.agh.mowgk.structures.NetStructure;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by mapu on 19/03/16.
 */
public class OffFileParser implements FileParser {
    private NetStructure myStructure;

    private String formatError = "OFF file format error.";
    public OffFileParser(){

    }

    public void loadFileIntoStructure(String filePath, NetStructure myStructure) throws IOException {
        this.myStructure = myStructure;

        File offFile = new File(filePath);
        if(!offFile.exists() || offFile.isDirectory()) {
            System.out.println("There is no such file.");
            return;
        }

        FileInputStream inputStream = null;
        Scanner scanner = null;
        try {
            inputStream = new FileInputStream(filePath);
            scanner = new Scanner(inputStream, "UTF-8");

            String line = null;
            if (scanner.hasNextLine()){
                line = scanner.nextLine();
            }

            if(line.compareTo("OFF") != 0) {
                System.out.println(formatError);
                return;
            }

            if(setInitialData(scanner) == false){
                System.out.println(formatError);
                return;
            }


            line = setVertexes(scanner);
            if(line == null){
                System.out.println(formatError);
                return;
            }

            if(!setFaces(scanner, line)){
                System.out.println(formatError);
                return;
            }


            // note that Scanner suppresses exceptions
            if (scanner.ioException() != null) {
                throw scanner.ioException();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (inputStream != null) {
                inputStream.close();
            }
            if (scanner != null) {
                scanner.close();
            }
        }

    }

    private boolean setFaces(Scanner scanner, String line) {
        if(!setFace(line)){
            return false;
        }

        while(scanner.hasNextLine()){
            line = scanner.nextLine();
            if(!setFace(line)){
                return false;
            }
        }

        return true;
    }

    private boolean setFace(String line){
        String[] vertices = line.split("\\s+");
        if(vertices.length != 4 | vertices[0].compareTo("3") != 0){
            return false;
        }

        int vertexIndice1 = Integer.parseInt(vertices[1]);
        int vertexIndice2 = Integer.parseInt(vertices[2]);
        int vertexIndice3 = Integer.parseInt(vertices[3]);
        myStructure.setFace(vertexIndice1, vertexIndice2, vertexIndice3);
        return true;
    }

    private String setVertexes(Scanner scanner) {
        String line = null;
        boolean newSection = false;
        Matcher matcher = null;

        Pattern fourIntegers = Pattern.compile("\\S+\\s+\\S+\\s\\S+\\s\\S+");

        while (scanner.hasNextLine() && !newSection) {
            line = scanner.nextLine();

            matcher = fourIntegers.matcher(line);
            newSection = matcher.matches();
            if(!newSection){
                String[] vertexValues = line.split("\\s+");
                if(vertexValues.length != 3){
                    return null;
                }

                double x = Double.parseDouble(vertexValues[0]);
                double y = Double.parseDouble(vertexValues[1]);
                double z = Double.parseDouble(vertexValues[2]);

                myStructure.setVertex(x, y, z);
            }

        }
        return line;
    }

    private boolean setInitialData(Scanner scanner) {
        String line = null;
        boolean newSection = false;
        while (scanner.hasNextLine() && !newSection) {
            line = scanner.nextLine();
            if(line.startsWith("#")){
                continue;
            }

            String[] initialValues = line.split(" ");
            if(initialValues.length != 3){
                return false;
            }

            int vertexCount = Integer.parseInt(initialValues[0]);
            int faceCount = Integer.parseInt(initialValues[1]);
            int edgeCount = Integer.parseInt(initialValues[2]);

            myStructure.setInitiatData(vertexCount, faceCount, edgeCount);
            newSection = true;
        }
        return true;
    }

}
