package pl.edu.agh.mowgk.parser;

import pl.edu.agh.mowgk.structures.NetStructure;

import java.io.IOException;

/**
 * Created by gaba on 19.03.16.
 */
public interface FileParser {
    void loadFileIntoStructure(String filePath, NetStructure structure) throws IOException;
}
