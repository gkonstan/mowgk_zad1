package pl.edu.agh.mowgk;

import pl.edu.agh.mowgk.parser.FileParser;
import pl.edu.agh.mowgk.parser.OffFileParser;
import pl.edu.agh.mowgk.parser.PlyFileParser;
import pl.edu.agh.mowgk.structures.NetStructure;
import pl.edu.agh.mowgk.structures.SimpleStructure;
import pl.edu.agh.mowgk.structures.WingedEdgesDS;

import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {
        System.out.println("Start.");
//        NetStructure structure = new SimpleStructure();
        NetStructure structure = new WingedEdgesDS();

//        String filePath = "meshFiles/simple.off";
//        filePath = "meshFiles/sphere2.off";
        String filePath = "meshFiles/flatNet.ply";
//        String filePath = "meshFiles/pyramid.ply";
//        String filePath = "meshFiles/cow.ply";
//        FileParser fileParser = new OffFileParser();
        FileParser fileParser = new PlyFileParser();
        fileParser.loadFileIntoStructure(filePath, structure);

        long startTime, stopTime;

        System.out.println("1.\t\t2.\t\t3.\t\t4.\t\t5.");
        for (int i = 0; i < 10; i++) {
            startTime = System.nanoTime();
                structure.findSurroundingVertexesForAllVertexes();
            stopTime = System.nanoTime();
            System.out.print(stopTime - startTime + "\t");

            startTime = System.nanoTime();
                structure.findFacesForAllVertexes();
            stopTime = System.nanoTime();
            System.out.print(stopTime - startTime + "\t");

            startTime = System.nanoTime();
                structure.findSurroundingFacesForAllFaces();
            stopTime = System.nanoTime();
            System.out.print(stopTime - startTime + "\t");

            startTime = System.nanoTime();
                structure.changeEdgesForTriangles(0, 5);
//            structure.changeEdgesForTriangles(0, 2);
//            structure.changeEdgesForTriangles(0, 64);  // sphere2
            stopTime = System.nanoTime();
            System.out.print(stopTime - startTime + "\t");

            startTime = System.nanoTime();
                structure.checkIfMeshHasMarrgin();
            stopTime = System.nanoTime();
            System.out.print(stopTime - startTime + "\t");

            System.out.println();
        }

    }
}
